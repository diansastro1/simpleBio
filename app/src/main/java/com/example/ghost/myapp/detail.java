package com.example.ghost.myapp;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


public class detail extends AppCompatActivity {

    ImageView imageView;
    private DBHelper mydb;
    int id_To_Update = 0;
    private Cursor c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().setTitle("Detail Biodata Mahasiswa");

        imageView = (ImageView) findViewById(R.id.imageView);
        final TextView textName = (EditText) findViewById(R.id.editText);
        final TextView textNim = (EditText) findViewById(R.id.editText2);
        final TextView textEmail = (EditText) findViewById(R.id.editText3);
        mydb = new DBHelper(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            int id = extras.getInt("id");


            if (id > 0){
                id_To_Update = id;
                c =  mydb.getData(id);
                c.moveToFirst();
                textName.setText(c.getString(c.getColumnIndex(DBHelper.COLUM_NAME)));
                textNim.setText(c.getString(c.getColumnIndex(DBHelper.COLUM_NIM)));
                textEmail.setText(c.getString(c.getColumnIndex(DBHelper.COLUM_EMAIL)));

                if ((textName.getText().length() != 0 )|| (textNim.getText().length() != 0) || (textEmail.getText().length() !=0)){

                    textName.setClickable(false);
                    textNim.setClickable(false);
                    textEmail.setClickable(false);
                }

                byte[] img = Base64.decode(c.getString(c.getColumnIndex(DBHelper.COLUM_FOTO)), Base64.DEFAULT);
                Bitmap image = BitmapFactory.decodeByteArray(img,0,img.length);
                imageView.setImageBitmap(image);
                Log.i("foto", img.toString());
            }
        }
    }


    public void kembali (View view){
        Intent intent = new Intent(detail.this, lihat.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(detail.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
