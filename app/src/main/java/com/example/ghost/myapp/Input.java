package com.example.ghost.myapp;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;

public class Input extends AppCompatActivity implements View.OnClickListener {

    //public static String  username= "username";
    //EditText nama;
    //EditText nim;
    //EditText email;
    Button save,browse;
    private ImageView imageView;
    private DBHelper mydb;
    private int PICK_IMAGE_REQUEST = 1;
    private Uri filepath;
    private Bitmap bitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        getSupportActionBar().setTitle("Input Data Mahasiswa");

        mydb = new DBHelper(this);
        imageView = (ImageView) findViewById(R.id.imageView2);
        final Random r = new Random();
        final EditText nama  = (EditText) findViewById(R.id.text1);
        final EditText nim   = (EditText) findViewById(R.id.text2);
        final EditText email = (EditText) findViewById(R.id.text3);
        save  = (Button) findViewById(R.id.button);
        browse = (Button) findViewById(R.id.button3);
        browse.setOnClickListener(this);
        save.setOnClickListener(this);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = r.nextInt(10);
                if (mydb.insert(id, nama.getText().toString(), nim.getText().toString(), email.getText().toString(),
                        getStringImage(bitmap).toString() )){
                    Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();
                    openDisplayAll();
                    finish();


                } else {
                    Toast.makeText(getApplicationContext(), "Gagal", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void showFileChooser(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Pilih Foto"), PICK_IMAGE_REQUEST);
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] imageBytes = stream.toByteArray();
        String encodeImages = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        Log.i("foto", encodeImages);
        return encodeImages;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK & data != null && data.getData() != null){
            filepath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
                imageView.setImageBitmap(bitmap);
                getStringImage(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(Input.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
         if (v == browse){
           showFileChooser();
        }
    }

    private void openDisplayAll() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
