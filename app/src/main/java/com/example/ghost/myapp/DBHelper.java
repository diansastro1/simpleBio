package com.example.ghost.myapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ghost on 03/11/16.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "biodata";
    public static final String  TB_NAME = "mahasiswa";
    public static final String COLUM_ID = "id";
    public static final String  COLUM_NAME = "nama";
    public static final String COLUM_NIM = "nim";
    public static final String COLUM_EMAIL = "email";
    public static final String COLUM_FOTO = "foto";
    public static final int DB_VERSION = 1;

    public DBHelper (Context  context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TB_NAME
                + "("
                + COLUM_ID + " INTEGER PRIMARY KEY,"
                + COLUM_NAME + " VARCHAR, "
                + COLUM_NIM  + " VARCHAR, "
                + COLUM_EMAIL + " VARCHAR, "
                + COLUM_FOTO + " VARCHAR);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS mahasiswa";
        db.execSQL(sql);
        onCreate(db);
    }

    public boolean insert(int id, String nama, String nim, String email, String foto){
        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUM_ID,id);
        values.put(COLUM_NAME, nama);
        values.put(COLUM_NIM,nim);
        values.put(COLUM_EMAIL,email);
        values.put(COLUM_FOTO,foto);

        db.insert(TB_NAME, null, values);
        db.close();
        return true;
    }

    public Cursor getData(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM mahasiswa WHERE id=" + id + ";";
        Cursor c = db.rawQuery(sql,null);
        return c;
    }

    public ArrayList<String> getAllData(){
        ArrayList<String> arrayList = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM mahasiswa ORDER BY id ASC ", null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            arrayList.add(cursor.getString(cursor.getColumnIndex(COLUM_ID))
                    + " . " +  cursor.getString(cursor.getColumnIndex(COLUM_NAME)));
            //arrayList.add(cursor.getString(cursor.getColumnIndex(COLUM_NAME)));
            cursor.moveToNext();
        }

        return arrayList;
    }

    public void hapus () {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DELETE FROM " + TB_NAME);
    }

}
