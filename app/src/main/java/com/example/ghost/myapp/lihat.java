package com.example.ghost.myapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import static android.R.layout.list_content;
import static android.R.layout.simple_list_item_1;

public class lihat extends AppCompatActivity {

    Button clear;
    private DBHelper mydb;
    int id_To_Update = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        getSupportActionBar().setTitle("Nama Mahasiswa");
        clear    = (Button) findViewById(R.id.button4);

        Bundle extras = getIntent().getExtras();

        mydb       = new DBHelper(this);
        ArrayList<String> array_list = mydb.getAllData();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, simple_list_item_1,array_list);

        final ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String UserInfo = listView.getItemAtPosition(position).toString();
                int id_To_Search = Integer.parseInt(UserInfo.substring(0, UserInfo.indexOf(" . ")));

                Bundle dataBundle = new Bundle();
                dataBundle.putInt("id", id_To_Search);

                Intent intent = new Intent(getApplicationContext(), detail.class);
                intent.putExtras(dataBundle);
                startActivityForResult(intent, 1);
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteData();
            }
        });
    }

    private void deleteData() {
        new AlertDialog.Builder(this)
                .setTitle("Hapus Data")
                .setMessage("Anda yakin ingin menghapus data ini?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mydb.hapus();

                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == RESULT_OK){
            Intent refresh = new Intent(this, lihat.class);
            startActivity(refresh);
            this.finish();
        }
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(lihat.this, MainActivity.class);
        startActivity(intent);
        finish();
    }


}
