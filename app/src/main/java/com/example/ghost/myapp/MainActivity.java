package com.example.ghost.myapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void input (View view){
        Intent intent = new Intent(MainActivity.this, Input.class);
        startActivity(intent);
        finish();
    }

    public void tampil (View v){
        Intent intent = new Intent(MainActivity.this, lihat.class);
        startActivity(intent);
        finish();
    }

}
